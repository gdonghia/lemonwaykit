class InvalidParameterError(Exception):
    pass


class ApiMethodError(Exception):
    def __init__(self, code, message, priority):
        self.code = code
        self.message = message
        self.priority = priority